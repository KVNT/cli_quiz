extern crate csv;
use log::error;
use std::collections::HashMap;
use std::error::Error;

#[derive(Debug)]
pub struct Card {
    pub question: String,
    pub answers: Vec<String>,
}

/// load_card_deck reads csv records into a HashMap of <String, String>
pub fn load_card_deck(file_path: &str) -> Result<HashMap<String, String>, Box<Error>> {
    let mut csv_reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_path(file_path)?;

    let mut card_map = HashMap::new();

    for (i, result) in csv_reader.records().enumerate() {
        let record = result?;
        // Questions reside in the first field of a row
        let question = record.get(0);
        if let Some("") | None = question {
            error!("Reading deck: Empty question field on line {}. The deck must be properly formatted!", i + 1);
            std::process::exit(1);
        }
        // Answers reside in the second field
        let answers = record.get(1);
        if let Some("") | None = answers {
            error!("Reading deck: Empty answers field on line {}. The deck must be properly formatted!", i + 1);
            std::process::exit(1);
        }

        card_map.insert(question.unwrap().to_owned(), answers.unwrap().to_owned());
    }
    return Ok(card_map);
}

/// make_cards creates a Vec of Cards out  of a map of <String. Vec<String>>
pub fn make_cards(card_map: HashMap<String, String>) -> Vec<Card> {
    let card_count = card_map.len();
    let mut cards = Vec::with_capacity(card_count);

    for (q, a) in card_map.iter() {
        let list_of_a: Vec<String> = a.split("|").map(|ans| ans.to_string()).collect();
        let card = Card {
            question: q.to_string(),
            answers: list_of_a,
        };
        cards.push(card);
    }

    return cards;
}

// shuffle_cards puts the contents of a Vec of Card types into a random order
pub fn shuffle_cards(cards: &mut Vec<Card>) {
    use rand::seq::SliceRandom;

    cards.shuffle(&mut rand::thread_rng());
}

// Displays the question of a card on stdout, takes user input and grades it
pub fn show_card(card: &Card) -> f64 {
    println!("Q: {}", card.question);

    let mut input = String::new();

    std::io::stdin()
        .read_line(&mut input)
        .expect("Could not read input");

    if input == "" {
        return -1f64;
    };

    max_score(&card.answers, &input.trim())
}

pub fn f() {
    unimplemented!() // TODO
}

extern crate edit_distance;
/// calculate_score calculates the score a given response has yielded against the correct answer
/// by determining the Levenshtein edit-distance between the two. The score will be a number between 0 and 100.
pub fn calculate_score(correct: &str, input: &str) -> f64 {
    let distance = edit_distance::edit_distance(correct, input);
    let score: f64 = 1.0 - distance as f64 / correct.len() as f64;
    if score < 0f64 {
        return 0f64;
    }
    (score * 100.0)
}

/// average_score returns the average of a series of scores
pub fn average_score(scores: &Vec<f64>) -> f64 {
    scores.iter().sum::<f64>() / scores.len() as f64
}

fn max_score(choices: &Vec<String>, input: &str) -> f64 {
    let max = choices
        .iter()
        .map(|choice| calculate_score(choice, input))
        .fold(std::f64::NAN, f64::max);

    max
}

