///
/// TODO :: Rewrite, RESTRUCTURE cards.rs, MAKE FUNCTIONAL; LIMIT SIDE EFFECTS
///
#[macro_use]
mod card;

use log::info;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "cli_quiz")]
struct Arguments {
    /// Set a limit on the maximum amount of cards to be loaded
    #[structopt(short = "l", long = "limit")]
    limit: Option<usize>,
    /// File from which to read the question/answers pairs
    #[structopt(name = "FILE")]
    path: String,
}

fn main() {
    use card::*;

    env_logger::init();
    info!("starting env_logger");

    let args = Arguments::from_args();
    let limit = args.limit.unwrap_or(20);

    // 1. Load card deck
    let deck = match load_card_deck("test.csv") {
        Ok(val) => val,
        Err(err) => panic!("Error while loading card deck: {}", err),
    };

    // 2. Make Cards (Vec<Card>) out of it
    let mut cards = make_cards(deck);

    // Shuffle the Vec of Cards
    shuffle_cards(&mut cards);

    // FOR AS LONG AS THERE ARE ELEMENTS LEFT IN Vec<Card> OR UNTIL SET LIMIT IS REACHED:
    println!(
        "{} cards loaded, {} is the limit of cards that will be shown",
        cards.len(),
        limit
    );

    // Show Card / Pose Question
    let mut scores = Vec::new();
    for (i, card) in cards.iter().enumerate() {
        if i == limit {
            break;
        }

        // Display a card
        let score = show_card(&card);

        // If answer wasn't 100% correct, display possible correct answers
        if score != 100f64 {
            print!("Not quite right! Possible answers: ");
            for (i, ans) in card.answers.iter().enumerate() {
                if i != 0 {
                    print!(" || ")
                }
                print!("{}", ans);
            }
            println!();
        }

        // Print and save the score
        println!("Score: {}", score);
        scores.push(score);

        // Wait until user presses a key before continuing
        // Alternatively I could use the ncurses library here...
        pause();
        clear();
    }

    // This implementation is HORRIBLE :D
    // None of the advantages and niceties of iterator combinators,
    // all of the pitfalls of a different imperative approach
    // let scores = cards
    //     .iter()
    //     .enumerate()
    //     .map(|c| card::show_card(c.1))
    //     .inspect(|score| println!("Your score: {:.0}.", score))
    //     .collect::<Vec<f64>>();

    println!("Your average score is {}", average_score(&scores));
    // println!("Scores {:?}", scores);
    // Take input and calculate score

    // Show correct answer and score

    // AFTER THAT:

    // Calculate and show average score
}

fn pause() {
    use std::io;
    use std::io::prelude::*;

    let mut stdin = io::stdin();
    let mut stdout = io::stdout();

    // We want the cursor to stay at the end of the line, so we print without a newline and flush manually.
    write!(stdout, "Press Enter to continue...").unwrap();
    stdout.flush().unwrap();

    // Read a single byte and discard
    let _ = stdin.read(&mut [0u8]).unwrap();
}

// TODO: Implement error handling!
fn clear() {
    if std::process::Command::new("clear")
        .status()
        .unwrap()
        .success()
    {
        println!("---");
    }
}